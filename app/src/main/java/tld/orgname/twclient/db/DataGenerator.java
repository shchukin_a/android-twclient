package tld.orgname.twclient.db;


import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;

import tld.orgname.twclient.db.entity.TaskEntity;

/**
 * Generates data to pre-populate the database
 */
public class DataGenerator {

    public static List<TaskEntity> generateTasks() {
        List<TaskEntity> tasks = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            TaskEntity task = new TaskEntity();
            task.setDescription(Faker.instance().book().title());
            task.setProject(Faker.instance().team().name());
            task.setTags(Faker.instance().color().name());
            tasks.add(task);
        }
        return tasks;
    }

}

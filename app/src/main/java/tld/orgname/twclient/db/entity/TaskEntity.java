package tld.orgname.twclient.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.github.javafaker.Faker;

import lombok.Getter;
import lombok.Setter;
import tld.orgname.twclient.model.Task;

/*
    [+] depends string Modifiable
    [+] description string Modifiable
    [+] due date Modifiable
    [+] end date Modifiable
    entry date Modifiable
    [+] id numeric Read
    imask numeric Read
    mask string Read
    modified date Modifiable
    parent string Read
    [+] priority string Modifiable
    [+] project string Modifiable
    recur string Modifiable
    scheduled date Modifiable
    [+] start date Modifiable
    status string Modifiable
    [+] tags string Modifiable
    [+] until date Modifiable
    urgency numeric Modifiable
    uuid string Read
    wait date Modifiable
*/

@Entity(tableName = "tasks")
public class TaskEntity implements Task {
    @PrimaryKey(autoGenerate = true)
    @Getter @Setter
    private int id;

    @ColumnInfo(name = "depends")
    @Getter @Setter
    private String depends;

    @ColumnInfo(name = "description")
    @Getter @Setter
    private String description;

    @ColumnInfo(name = "due")
    @Getter @Setter
    private String due;

    @ColumnInfo(name = "end")
    @Getter @Setter
    private String end;

    @ColumnInfo(name = "priority")
    @Getter @Setter
    private String priority;

    @ColumnInfo(name = "project")
    @Getter @Setter
    private String project;

    @ColumnInfo(name = "start")
    @Getter @Setter
    private String start;

    @ColumnInfo(name = "tags")
    @Getter @Setter
    private String tags;

    @ColumnInfo(name = "until")
    @Getter @Setter
    private String until;

    public TaskEntity() {
    }
}

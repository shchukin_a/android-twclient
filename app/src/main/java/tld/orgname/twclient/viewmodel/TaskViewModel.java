package tld.orgname.twclient.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;

import tld.orgname.twclient.App;
import tld.orgname.twclient.DataRepository;
import tld.orgname.twclient.db.entity.TaskEntity;


public class TaskViewModel extends AndroidViewModel {

    private final LiveData<TaskEntity> mObservableTask;

    public ObservableField<TaskEntity> task = new ObservableField<>();

    private final int mTaskId;

    public TaskViewModel(@NonNull Application application, DataRepository repository, final int taskId) {
        super(application);
        mTaskId = taskId;

        mObservableTask = repository.getTaskById(mTaskId);
    }

    public LiveData<TaskEntity> getObservableTask() {
        return mObservableTask;
    }

    public void setTask(TaskEntity task) {
        this.task.set(task);
    }

    /**
     * A creator is used to inject the task ID into the ViewModel
     * <p>
     * This creator is to showcase how to inject dependencies into ViewModels. It's not
     * actually necessary in this case, as the task ID can be passed in a public method.
     */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {

        @NonNull
        private final Application mApplication;

        private final int mTaskId;

        private final DataRepository mRepository;

        public Factory(@NonNull Application application, int taskId) {
            mApplication = application;
            mTaskId = taskId;
            mRepository = ((App) application).getRepository();
        }

        @Override
        public <T extends ViewModel> T create(Class<T> modelClass) {
            //noinspection unchecked
            return (T) new TaskViewModel(mApplication, mRepository, mTaskId);
        }
    }
}

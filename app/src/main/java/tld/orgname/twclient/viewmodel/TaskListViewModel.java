package tld.orgname.twclient.viewmodel;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;

import java.util.HashSet;
import java.util.List;

import tld.orgname.twclient.App;
import tld.orgname.twclient.db.entity.TaskEntity;


public class TaskListViewModel extends AndroidViewModel {

    // MediatorLiveData can observe other LiveData objects and react on their emissions.
    private final MediatorLiveData<List<TaskEntity>> mObservableTasks;
    private final MediatorLiveData<HashSet<Integer>> mObservableCheckedIds;

    public TaskListViewModel(Application application) {
        super(application);

        mObservableTasks = new MediatorLiveData<>();
        mObservableCheckedIds = new MediatorLiveData<>();
        // set by default null, until we get data from the database.
        mObservableTasks.setValue(null);
        mObservableCheckedIds.setValue(null);

        LiveData<List<TaskEntity>> tasks = ((App) application).getRepository().getAllTasks();

        // observe the changes of the tasks from the database and forward them
        mObservableTasks.addSource(tasks, mObservableTasks::setValue);
    }

    /**
     * Expose the LiveData Tasks query so the UI can observe it.
     */
    public LiveData<List<TaskEntity>> getAllTasks() {
        return mObservableTasks;
    }
    public LiveData<HashSet<Integer>> getCheckedTaskIds() {
        return mObservableCheckedIds;
    }
}

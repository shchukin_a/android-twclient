package tld.orgname.twclient.model;

public interface Task {
    int getId();
    String getDepends();
    String getDescription();
    String getDue();
    String getEnd();
    String getProject();
    String getPriority();
    String getStart();
    String getTags();
    String getUntil();
}

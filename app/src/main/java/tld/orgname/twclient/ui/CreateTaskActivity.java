package tld.orgname.twclient.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tld.orgname.twclient.R;

public class CreateTaskActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_task);
    }
}

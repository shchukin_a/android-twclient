package tld.orgname.twclient.ui;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.HashSet;
import java.util.List;

import tld.orgname.twclient.R;
import tld.orgname.twclient.databinding.ItemTaskBinding;
import tld.orgname.twclient.model.Task;


public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> {

    private final String TAG = this.getClass().getName();
    private List<? extends Task> mTaskList;

    private HashSet<Integer> mCheckedTasks;

    @Nullable
    private final TaskClickCallback mTaskClickCallback;

    TaskAdapter(@Nullable TaskClickCallback clickCallback) {
        mTaskClickCallback = clickCallback;
    }

    public void setTaskList(final List<? extends Task> taskList) {
        Log.v(TAG, "setTaskList");
        if (mTaskList == null) {
            mTaskList = taskList;
            notifyItemRangeInserted(0, taskList.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mTaskList.size();
                }

                @Override
                public int getNewListSize() {
                    return taskList.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return mTaskList.get(oldItemPosition).getId() ==
                            taskList.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Task newTask = taskList.get(newItemPosition);
                    Task oldTask = mTaskList.get(oldItemPosition);
                    return newTask.getId() == oldTask.getId()
                            && newTask.getDescription().equals(oldTask.getDescription());
                }
            });
            mTaskList = taskList;
            result.dispatchUpdatesTo(this);
        }
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.v(TAG, "onCreateViewHolder");
        ItemTaskBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_task,
                parent,
                false);
        binding.setCallback(mTaskClickCallback);
        return new TaskViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        holder.binding.setTask(mTaskList.get(position));
        holder.binding.executePendingBindings();

        holder.binding.cbTask.setOnCheckedChangeListener(null);
        if (mCheckedTasks.contains(holder.binding.getTask().getId())) {
            holder.binding.cbTask.setChecked(true);
        }

        holder.binding.cbTask.setOnCheckedChangeListener((buttonView, isChecked) -> {
            int taskId = holder.binding.getTask().getId();

            if (isChecked) {
                mCheckedTasks.add(taskId);
                Log.v(TAG, String.format("task id '%s' has been added to set", taskId));
            } else {
                mCheckedTasks.remove(taskId);
                Log.v(TAG, String.format("task id '%s' has been removed from set", taskId));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTaskList == null ? 0 : mTaskList.size();
    }

    public void setCheckedTasks(HashSet<Integer> mCheckedTasks) {
        this.mCheckedTasks = mCheckedTasks;
    }

    public HashSet<Integer> getCheckedTasks() {
        return mCheckedTasks;
    }

    static class TaskViewHolder extends RecyclerView.ViewHolder {

        final ItemTaskBinding binding;

        private TaskViewHolder(ItemTaskBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}


package tld.orgname.twclient.ui;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashSet;

import tld.orgname.twclient.R;
import tld.orgname.twclient.databinding.FragmentTasksListBinding;
import tld.orgname.twclient.viewmodel.TaskListViewModel;


public class TaskListFragment extends Fragment {

    public static String TAG = "TaskListFragment";

    private TaskAdapter mTaskListAdapter;
    private HashSet<Integer> mCheckedTasks = new HashSet<>();

    private FragmentTasksListBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Log.v(TAG, "onCreateView");
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_tasks_list, container, false);

        mTaskListAdapter = new TaskAdapter(mTaskClickCallback);
        mBinding.tasksList.setAdapter(mTaskListAdapter);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.v(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        final TaskListViewModel viewModel =
                ViewModelProviders.of(this).get(TaskListViewModel.class);

        subscribeUi(viewModel);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mCheckedTasks != null) {
            mTaskListAdapter.setCheckedTasks(mCheckedTasks);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mCheckedTasks = mTaskListAdapter.getCheckedTasks();
    }

    private void subscribeUi(TaskListViewModel viewModel) {
        // Update the list when the data changes
        Log.v(TAG, "subscribeUi");
        viewModel.getAllTasks().observe(this, tasks -> {
            if (tasks != null) {
                mBinding.setIsLoading(false);
                mTaskListAdapter.setTaskList(tasks);
            } else {
                mBinding.setIsLoading(true);
            }
            // espresso does not know how to wait for data binding's loop so we execute changes sync.
            mBinding.executePendingBindings();
        });

    }

    private final TaskClickCallback mTaskClickCallback = task -> {

        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
            ((MainActivity) getActivity()).show(task);
        }
    };

}

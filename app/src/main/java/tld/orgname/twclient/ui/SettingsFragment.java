package tld.orgname.twclient.ui;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import tld.orgname.twclient.R;

public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}

package tld.orgname.twclient.ui;

import tld.orgname.twclient.model.Task;


public interface TaskClickCallback {
    void onClick(Task task);
}

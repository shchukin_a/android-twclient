package tld.orgname.twclient.ui;

import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tld.orgname.twclient.R;
import tld.orgname.twclient.databinding.FragmentTaskBinding;
import tld.orgname.twclient.db.entity.TaskEntity;
import tld.orgname.twclient.viewmodel.TaskViewModel;


public class TaskFragment extends Fragment {

    private static final String KEY_TASK_ID = "task_id";

    private FragmentTaskBinding mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate this data binding layout
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_task, container, false);

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TaskViewModel.Factory factory = new TaskViewModel.Factory(
                getActivity().getApplication(),
                getArguments().getInt(KEY_TASK_ID)
        );

        final TaskViewModel model = ViewModelProviders.of(this, factory).get(TaskViewModel.class);

        mBinding.setTaskViewModel(model);

        subscribeToModel(model);
    }

    private void subscribeToModel(final TaskViewModel model) {

        // Observe task data
        model.getObservableTask().observe(this, new Observer<TaskEntity>() {
            @Override
            public void onChanged(@Nullable TaskEntity taskEntity) {
                model.setTask(taskEntity);
            }
        });

    }

    /** Creates task fragment for specific task ID */
    public static TaskFragment forTask(int taskId) {
        TaskFragment fragment = new TaskFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_TASK_ID, taskId);
        fragment.setArguments(args);
        return fragment;
    }
}

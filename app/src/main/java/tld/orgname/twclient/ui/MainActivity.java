package tld.orgname.twclient.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import tld.orgname.twclient.R;
import tld.orgname.twclient.model.Task;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    SharedPreferences sharedPrefs;
    private final String TAG = this.getClass().getName();

    public static final String PREFS_NAME = "TWClient";
    public static final String PREF_LIGHT_THEME = "use_light_theme";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPrefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        boolean isLightThemePreferable = sharedPrefs.getBoolean("use_light_theme", false);
        if (isLightThemePreferable) {
            setTheme(R.style.AppTheme_Light_NoActionBar);
        }
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener((View view) -> {
            Intent intent = new Intent(this, CreateTaskActivity.class);
            startActivity(intent);
        });

        if (savedInstanceState != null) {
            Fragment savedFragment =  getSupportFragmentManager().getFragment(savedInstanceState, "current_fragment");
            replaceFragmentTo(savedFragment);
        } else {
            TaskListFragment tasks = new TaskListFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, tasks, TaskListFragment.TAG).commit();
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        getSupportFragmentManager().putFragment(outState,"current_fragment", currentFragment);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_tasks:
                TaskListFragment tasks = new TaskListFragment();
                replaceFragmentTo(tasks);
                break;
            case R.id.nav_tags:
                break;
            case R.id.nav_settings_general:
                Snackbar.make(
                        findViewById(R.id.fragment_container),
                        "TODO: show settings",
                        Snackbar.LENGTH_LONG).show();
                // SettingsFragment settings = new SettingsFragment();
                // replaceFragmentTo(settings);
                break;
            case R.id.nav_theme:
                switchTheme();
                break;
            default:
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void switchTheme() {
        sharedPrefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPrefs.edit();

        boolean isLightThemePreferable = sharedPrefs.getBoolean(PREF_LIGHT_THEME, false);
        prefsEditor.putBoolean(PREF_LIGHT_THEME, !isLightThemePreferable);
        prefsEditor.apply();

        recreate();
    }

    private void replaceFragmentTo(Fragment fragment) {
        FragmentManager ft = getSupportFragmentManager();
        ft.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    /** Shows the task detail fragment */
    public void show(Task task) {

        TaskFragment taskFragment = TaskFragment.forTask(task.getId());

        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("task")
                .replace(R.id.fragment_container, taskFragment, null)
                .commit();
    }
}

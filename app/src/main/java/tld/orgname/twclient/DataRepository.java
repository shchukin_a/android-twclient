package tld.orgname.twclient;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;

import java.util.List;

import tld.orgname.twclient.db.AppDatabase;
import tld.orgname.twclient.db.entity.TaskEntity;

/**
 * Repository handling the work with tasks and comments.
 */
public class DataRepository {

    private static DataRepository sInstance;

    private final AppDatabase mDatabase;
    private MediatorLiveData<List<TaskEntity>> mObservableTasks;

    private DataRepository(final AppDatabase database) {
        mDatabase = database;
        mObservableTasks = new MediatorLiveData<>();

        mObservableTasks.addSource(mDatabase.taskDao().getAllTasks(),
                taskEntities -> {
                    if (mDatabase.getDatabaseCreated().getValue() != null) {
                        mObservableTasks.postValue(taskEntities);
                    }
                });
    }

    public static DataRepository getInstance(final AppDatabase database) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(database);
                }
            }
        }
        return sInstance;
    }

    /**
     * Get the list of tasks from the database and get notified when the data changes.
     */
    public LiveData<List<TaskEntity>> getAllTasks() {
        return mObservableTasks;
    }

    public LiveData<TaskEntity> getTaskById(final int taskId) {
        return mDatabase.taskDao().getTaskById(taskId);
    }

}
